# -*- coding: utf-8 -*-
"""
Materiał do laboratorium z przedmiotu Przetwarzanie multimediów
PG, 2019/2020
"""


# =============================================================================
# Tworzenie funkcji
# =============================================================================
import numpy as np


def foo(a, b, c):
    return a+b+c


# Przyklad tworzenie funkcji z notką dokumentacyjną ("docstring")
# Sprawdz, ze notka dokumentacyjna jest wyswietlana podczas introspekcji obiektu
# ?foo2
def foo2(mat):
    """
    Funkcja wykonuje dodanie do macierzy 3x3 podanej jako arguemnt 
    macierzy jedynek
    ---
    Uwaga: funkcja nie bada poprawnosci przekazanego argument
    """
    jedynki = np.ones((3, 3))
    return mat + jedynki


# foo2(np.ones((3, 3)))

# Wariant funkcji foo2 z asercją


def foo2(mat):
    """
    Funkcja wykonuje dodanie do macierzy 3x3 podanej jako arguemnt 
    macierzy jedynek
    """
    assert mat.shape == (3, 3), "Niepoprawny wymiary macierzy. Koniecznie 3x3"
    jedynki = np.ones((3, 3))
    return mat + jedynki


# test
# foo2(np.ones((4, 4)))
# foo2(np.ones((3, 3)))


# =============================================================================
# Zadanie. Gra w zycie (life game)
# Przeprowadź symulację gry w życie wg reguł Conwaya (https://pl.wikipedia.org/wiki/Gra_w_%C5%BCycie)
# Rozpatrz stany początkowe planszy zawierające obok rozkładu losowego struktury oscylatorów, np. statki (spaceships).
# Postać planszy po każdym etapie zapisz w pliku jpg.
# Przyjmij, że inicjalny stan planszy jest losowy (np. komórka żywa z prawdopodobieństwem 0.1)
# Plansza powinna mieć wymiar co najmniej 100x100.
# Podpowiedź: do zapisu obrazu możesz wykorzystać instrukcję imsave z pakietu matplotlib.pyplot
#
# =============================================================================

# 
# Rafał Klifeld 167883
# 

import os
import random
import matplotlib.pyplot as plt

class GameOfLife():
    def __init__(self, gridSize=100, step=25, mode=0):
        self.gridSize = 100 if gridSize < 100 else gridSize
        self.step = 25 if step < 1 else step
        self.mode = mode
        # 
        # mode 0: Random grid
        # mode 1: Spaceship
        # mode 2: Random grid + Spaceship
        #  
       
    def __spaceship(self):
        grid = np.zeros((self.gridSize, self.gridSize))
        m = 4
        grid[10*m, 11*m] = 1
        grid[10*m, 14*m] = 1
        grid[11*m:13*m, 10] = 1
        grid[12*m, 14*m] = 1
        grid[13*m, 10:14*m] = 1
        return grid.astype(int)

    def __generateGrid(self):
        return (np.random.rand(self.gridSize**2) < 0.1).astype(int).reshape((self.gridSize, self.gridSize))

    def __gridMode(self):
        if self.mode == 0:
            grid = self.__generateGrid()
        elif self.mode == 1:
            grid = self.__spaceship()
        elif self.mode == 2:
            grid = self.__generateGrid() + self.__spaceship()
        else:
            grid = self.__generateGrid()

        return grid

    def __countNeighbours(self, grid):
        res = np.zeros(grid.shape)
        temp = np.roll(np.roll(grid, -1, axis=0), -1, axis=1)

        for i in range(3):
            for j in range(3):
                if i == 1 and j == 1:
                    pass
                else:
                    res += np.roll(temp, j, axis=1)
            temp = np.roll(temp, 1, axis=0)
        
        return res.astype(int)

    def __conwayRules(self, grid, neighbours):
        return np.logical_or(np.logical_and(grid, neighbours == 2), neighbours == 3).astype(int)

    def __saveToPic(self, grid, step):
        if not os.path.exists('./life'):
            os.makedirs('./life')
        plt.imsave('./life/life'+str(step+1)+'.png',grid)

    def run(self):
        grid = self.__gridMode()
        for i in range(self.step):
            neighbours = self.__countNeighbours(grid)
            grid = self.__conwayRules(grid, neighbours)
            self.__saveToPic(grid, i)
             


GameOfLife(gridSize=250, step=10, mode=2).run()