# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
####################################################
# Materiały do wykładu "Przetwarzanie multimediów".
# Laboratorium 1
# Autor: Marcin Wilczewski
# semestr zimowy 2019/2020
####################################################

import random

#
# Rafał Klifeld 167883
#

# Zadanie 1
# Korzystając z wyrażeń listowych (list comprehensions) utwórz listę 100 liczb podzielnych przez 7


def zad1():
    arr = [i*7 for i in range(1, 101)]
    print('arr len:', len(arr))
    print('arr:', arr)


# zad1()

# Zadanie 2
# Utwórz listę liczb nieparzystych. Lista dowolnej długosci.


def zad2():
    arr = [i*2+1 for i in range(0, 10)]
    print('arr:', arr)


# zad2()

# Zadanie 3
# Korzystając z list comprehension z napisu (string) wydobądż wszystkie cyfry


def zad3():
    s = 'ef846efw6 684wef fw8efwef88323'
    print([int(i) for i in ''.join((ch if ch.isdigit() else ' ')
                                   for ch in s).split()])


# zad3()

# Zadanie 4
# Stworz ciag reprezentujacy kolejne elementy ciagu fibonaciego: 0, 1, f_[n-1]+f_[n-2]


def zad4():
    f_ = [0, 1]
    for i in range(2, 10):
        f_.append(f_[i-1]+f_[i-2])
    print(f_)


# zad4()

# Zadanie 5
# Utworz liste wszystkich 676 par liter alfabetu aa..zz


def zad5():
    alfabet = 'abcdefghijklmnopqrstuvwxyz'
    alfabet_pairs = []

    for x in alfabet:
        for y in alfabet:
            alfabet_pairs.append(x + y)
    print('Number of pairs:', len(alfabet_pairs))
    print('pairs:', alfabet_pairs)


# zad5()

# Zadanie 6
# Napisz prosty generator haseł zawierającyuch znaki alfanumeryczne. Długosc hasła: min = 8, maks. 12 znaków.
# Kod powinien umożliwiać po jednym uruchomieniu uzyskanie jednego hasła w postaci listy.
# Wskazówka: hasło powinno składać się z losowo wybranych elementów wczesniej zdefiniowanego słownika (znaki alfanumeryczne).


def zad6():
    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    password = ''
    for x in range(random.randrange(8, 13)):
        password += random.choice(chars)
    print('password:', password)


# zad6()


def run():
    print('Zadanie 1:')
    zad1()
    print('\nZadanie 2:')
    zad2()
    print('\nZadanie 3:')
    zad3()
    print('\nZadanie 4:')
    zad4()
    print('\nZadanie 5:')
    zad5()
    print('\nZadanie 6:')
    zad6()


run()
