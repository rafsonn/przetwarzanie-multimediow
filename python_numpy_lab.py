# -*- coding: utf-8 -*-
"""

Laboratorium z Przetwarzanie multimediów
sem. 2019/2020
Autor: Marcin Wilczewski
"""
# =============================================================================
# 
# 1. http://www.scipy-lectures.org/intro/numpy/array_object.html
# 2. http://www.scipy-lectures.org/intro/matplotlib/index.html
# 3. ftp://ftp.cea.fr/pub/unati/people/educhesnay/pystatml/StatisticsMachineLearningPythonDraft.pdf
# 
# =============================================================================
# 
# Rafał Klifeld 167883
# 

import numpy
import matplotlib.pyplot as plt

is_even = lambda val: (val & 1) == 0
is_odd = lambda val: (val & 1) == 1

#Zadanie 1
#Narysuj wykres funkcji sin(x) oraz sin(x) + epsilon(x), gdzie epsilon(x) jest liczbą losową (rozkład jednostajny)
# z przedzialu 0..0.1
#Uwaga: skorzystaj z numpy.random.rand

def zad1():
    x = numpy.arange(-2*numpy.pi, 2*numpy.pi,0.05)
    y = numpy.sin(x)
    epsilon = (numpy.random.rand() / 10.0)
    y2 = numpy.sin(x) + epsilon
    plt.plot(x,y,'r',label='f(x) = sin(x)')
    plt.plot(x,y2,'g', label='f(x) = sin(x) + epsilon(x)')
    plt.xlabel('epsilon(x) = {}'.format(epsilon))
    plt.legend(loc='upper right', shadow=True, fontsize='x-small')

    plt.show()

# zad1()

#Zadanie 2
#Przeprowadź 10..10000 z krokiem 10 eksperymentow rzutu kostką szescienna (wybor liczby losowej z przedzialu 1..6).
#W kazdym eksperymencie okresl czestosc (pdp) wyrzucenia okreslonej liczby oczek, np. "1". 
#Czestosc w funkcji liczby oczek nanies na wykres.

def percentage(percent, whole):
    return (percent * 100.0) / whole

def zad2():
    SELECTED_NUMBER = 4
    pdp = list()
    nr_throws = numpy.arange(10,10001,10)
    
    for i in nr_throws:
        # Experiment
        res = sum(map(lambda val: val == SELECTED_NUMBER, numpy.random.randint(1, high=7, size=i)))    
        pdp.append(percentage(res,i))

    plt.plot(range(len(nr_throws)), pdp)
    plt.ylabel('Procentage of draw {}'.format(SELECTED_NUMBER))
    plt.xlabel('No. experiment')
    plt.show()

# zad2()

#Zadanie 3
#Utworz wektor 1000 liczb losowych z przedzialu 1..100, a nastepnie oblicz niezaleznie liczbę elementów parzystych i nieparzystych
def zad3():
    arr = numpy.random.randint(1,high=101,size=1000)
    even = sum(map(lambda val: is_even(val),arr))
    odd = sum(map(lambda val: is_odd(val),arr))
    print('Even numbers:', even)
    print('Odd numbers:', odd)
    
# zad3()

#Zadanie 4
#Utworz macierz 5x5 liczb losowych z przedzialu 0..9.
#a nastepnie stworz nowa macierz, w ktorej elmenty parzyste wyjsciowej macierzy zastap 1, a nieparzyste 0
def zad4():
    matrix1 = numpy.random.randint(10,size=(10, 10))
    matrix2 = numpy.vectorize(lambda val: 1 if is_even(val) else 0)(matrix1)
    print('Before:\n', matrix1)
    print('After:\n', matrix2)

# zad4()

#Zadanie 5
#Utworz macierz 10x10 liczb losowych z przedzialu 0..9, 
#a nastepnie skrajne podmacierze 3x3 (gorny lewy rog, gorny prawy rog, itd) zastap macierzą 1
def zad5():
    MATRIX_SIZE = 10
    SUBMATRIX_SIZE = 3
    matrix1 = numpy.random.randint(10,size=(MATRIX_SIZE, MATRIX_SIZE))
    ones = numpy.ones((SUBMATRIX_SIZE, SUBMATRIX_SIZE))
    
    print('Before:\n', matrix1)
    matrix1[0:SUBMATRIX_SIZE, 0:SUBMATRIX_SIZE] = ones
    matrix1[0:SUBMATRIX_SIZE, MATRIX_SIZE-SUBMATRIX_SIZE:MATRIX_SIZE] = ones
    matrix1[MATRIX_SIZE-SUBMATRIX_SIZE:MATRIX_SIZE, 0:SUBMATRIX_SIZE] = ones
    matrix1[MATRIX_SIZE-SUBMATRIX_SIZE:MATRIX_SIZE, MATRIX_SIZE-SUBMATRIX_SIZE:MATRIX_SIZE] = ones
    print('After:\n', matrix1)

# zad5()

#Zadanie 6
#Wygeneruj dwa wektory liczb losowych. Następnie wyznacz ich sumę oraz iloczyn skalarny.
def zad6():
    arr1 = numpy.random.randint(10, size=15)
    arr2 = numpy.random.randint(10, size=15)
    arr3 = numpy.sum([arr1, arr2], axis=0)
    arr4 = numpy.dot(arr1, arr2)
    print('Sum:')
    print('{} + {} = {}'.format(arr1,arr2,arr3))
    print('Scalar product:')
    print('{} ∘ {} = {}'.format(arr1,arr2,arr4))

# zad6()

def run():
    print('Zadanie 1:')
    zad1()
    print('\nZadanie 2:')
    zad2()
    print('\nZadanie 3:')
    zad3()
    print('\nZadanie 4:')
    zad4()
    print('\nZadanie 5:')
    zad5()
    print('\nZadanie 6:')
    zad6()


run()