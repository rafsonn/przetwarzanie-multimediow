# -*- coding: utf-8 -*-
"""
Widzenie komputerowe 
sem. 2019/2020
Autor: Marcin Wilczewski
"""


# OBRAZY


# Przykład 1
# Wczytaj przykladowy obraz i do kazdego piksela takiego obrazu dodaj szum maksymalny (=255 w kazdym kanale)
# z prawdopodobienstwem 0.1
import matplotlib.pyplot as plt
import numpy as np
# %matplotlib qt

# img = plt.imread('butterfly.png')
# plt.imshow(img, aspect='auto')
# plt.colorbar()
# plt.show()

# img2 = img.copy()
# for i in range(1, img2.shape[0]):
#     for j in range(1, img2.shape[1]):
#         if np.random.rand() <= 0.1:
#             img2[i, j, :] = 255

# plt.imshow(img2)
# plt.show()
# KONIEC


# Przykład 2
# Podobnie jak wyzej, ale dodaj szum bialy, tzn. piksel zastap pikselem (x,y,z) gdzie x,y,z sa liczbami losowymi
# z przedzialu 0..255

# plt.close()
# img = plt.imread('butterfly.png')
# plt.imshow(img, aspect='auto')
# plt.colorbar()
# plt.show()

# img2 = img.copy()
# for i in range(1, img2.shape[0]):
#     for j in range(1, img2.shape[1]):
#         if np.random.rand() <= 0.3:
#             img2[i, j, 0] = np.random.randint(0, 256)  # R
#             img2[i, j, 1] = np.random.randint(0, 256)  # G
#             img2[i, j, 2] = np.random.randint(0, 256)  # B

# plt.imshow(img2)
# plt.show()

# KONIEC


# =============================================================================
# Zadania (4 zadania = 2 punkty lab)
# =============================================================================

# Zadanie 1 (1/2 punktu)
# Wykonaj filtr wygladzajacy (usredniający) dla roznych rozmiarów masek (np. 3x3 oraz 11x11).
# Nie korzystaj z gotowych funkcji bibliotecznych. Zrealizuj to poprzez bezposrednie oprogramowanie "piksel po pikselu".
# W celu przypomnienia czym są filtry tego typu zajrzyj do materiałów wykładowych. Wszystko co niezbędne zostało
# tam opisane.
# Wyniki filtracji zapisz w pliku (plt.imsave("nazwa_pliku",obiekt_obrazu))

INPUT_FILE = 'butterfly.png'


def add(arr, offset=0):
    return np.pad(arr, pad_width=((offset, offset), (offset, offset), (0, 0)), mode='constant', constant_values=0)


def trim(arr, mask):
    bounding_box = tuple(
        slice(np.min(indexes), np.max(indexes) + 1)
        for indexes in np.where(mask))
    return arr[bounding_box]


def zad1(img, size=3):
    mask = np.full((size, size), 1/size**2)

    img = plt.imread(img)

    offset = len(mask) // 2

    img = add(img, offset)

    img2 = img.copy()

    for x in range(img.shape[0]-offset):
        for y in range(img.shape[1]-offset):
            acc = [0, 0, 0]
            for i in range(len(mask)):
                for j in range(len(mask)):
                    xn = x + i - offset
                    yn = y + j - offset
                    acc[0] += img[xn, yn, 0] * mask[i][j]
                    acc[1] += img[xn, yn, 1] * mask[i][j]
                    acc[2] += img[xn, yn, 2] * mask[i][j]

            img2[x, y, 0] = acc[0]  # R
            img2[x, y, 1] = acc[1]  # G
            img2[x, y, 2] = acc[2]  # B

    img2 = trim(img2, img2 != 0)

    plt.imsave("zad1-{}x{}.png".format(size, size), img2)
    print('DONE')


zad1(INPUT_FILE, 3)
zad1(INPUT_FILE, 11)


# Zadanie 2 (1/2 punktu)
# Tak samo jak w zadaniu wyżej, ale filtr medianowy.

def zad2(img, size=3):
    img = plt.imread(img)

    offset = size // 2

    img = add(img, offset)

    img2 = img.copy()

    for x in range(offset, img.shape[0]-offset):
        for y in range(offset, img.shape[1]-offset):
            acc = [[], [], []]
            for i in range(size):
                for j in range(size):
                    xn = x + i - offset
                    yn = y + j - offset
                    acc[0].append(img[xn, yn, 0])
                    acc[1].append(img[xn, yn, 1])
                    acc[2].append(img[xn, yn, 2])

            acc = np.median(acc, axis=1)
            img2[x, y, 0] = acc[0]  # R
            img2[x, y, 1] = acc[1]  # G
            img2[x, y, 2] = acc[2]  # B

    img2 = trim(img2, img2 != 0)

    plt.imsave("zad2-{}x{}.png".format(size, size), img2)
    print('DONE')


zad2(INPUT_FILE, 3)
zad2(INPUT_FILE, 11)


# Zadanie 3 (1/2 punktu)
# Wykonaj efekt "bathroom window effect"
# Efekt jest realizowany przez filtrację, gdzie i,j piksel (w każdym kanale barwnym) jest transformowany
# w następujący sposób:
#  f(i,j,kanał ) = f(i+(i%10)-4,j+(j%10)-4,kanał)
# Uwaga: to zadanie różni się od poprzednich wyłącznie sposobem wyznaczenia nowej wartosci przetwarzanego piksela.
# W tym przypadku zamiast wyznaczać srednią lub medianę z sąsiedztwa realizowana jest funkcja podana wyżej.

def zad3(img):
    img = plt.imread(img)
    img2 = img.copy()

    def f(i): return i+(i % 10)-4
    def change_check(i, r): return f(i) if f(i) < r else i

    x_range = img.shape[0]
    y_range = img.shape[1]

    for x in range(x_range):
        for y in range(y_range):
            img2[x, y, 0] = img[change_check(
                x, x_range), change_check(y, y_range), 0]  # R
            img2[x, y, 1] = img[change_check(
                x, x_range), change_check(y, y_range), 1]  # G
            img2[x, y, 2] = img[change_check(
                x, x_range), change_check(y, y_range), 2]  # B

    plt.imsave("zad3.png", img2)
    print('DONE')


zad3(INPUT_FILE)


# Zadanie 4 (1/2 punktu)
# Wykonaj transformację obrazu cyfrowego w taki sposób, by wartoć każdego parzystego piksela (przeglądając wiersz po wierszu)
# zastąpić wartoscią piksela nieprzystego, bezposrednio go poprzedzającego. Przykład: w danym wierszu obrazu piksel nr 2
# ma przyjąc wartosc piksela nr 1, piksel nr 4 - piksela nr 3, piksel nr 6 - piksela nr 5, itd.
# Taka procedura realizuje skrajnie uproszczoną #metodę kompresji danych obrazowych: około połowy pikseli
# oryginalnych zostaje zastąpionych pikselami sąsiednimi. Czy jest to kompresja? Tak, ponieważ znając taki protokół
# "nadawca" może wysłać do "odbiorcy" wyłącznie piksele nieparzyste oraz wymiary obrazu. "Odbiorca" jest w stanie
# obraz poprawnie zdekompresować poprzez uzupełnienie wartosci brakujących pikseli.
# Uwagi:
# 1. Liczenie pikseli (parzysty, nieparzysty) realizuje niezależnie dla każdego wiersza obrazu.
# 2. Ewentualny problem ostatniego piksela w wierszu (pojawi się gdy szerokosc obrazu jest nieparzysta) rozwiąż dowolnie.
#   Najprosciej: zignoruj - nie ma piksela parzystego po ostatnim pikselu nieparzystym.

def zad4(img):
    img = plt.imread(img)

    for x in range(img.shape[1]):
        if x % 2 == 0 and x < img.shape[1]-1:
            img[:, x+1] = img[:, x]

    plt.imsave("zad4.png", img)
    print('DONE')


zad4(INPUT_FILE)
